<?php
header('Content-type: application/xml');
/**
 * Created by PhpStorm.
 * User: Paweł
 * Date: 2017-05-30
 * Time: 13:37
 */

require_once('DB.php');

$DB = new DB('localhost', 'root', '', 'phpcamp_pkolasinski');

if(htmlspecialchars($_GET["action"]) == 'checkProduct')
{
    $DB->query('SELECT * FROM `products` WHERE id = '. $_GET["id"] .';');
    //echo json_encode($DB->getAllRows());
}

if(htmlspecialchars($_GET["action"]) == 'addProduct')
{
    $DB->query('INSERT INTO `products` (name, price) VALUES ("'. htmlspecialchars($_GET["name"]). '","'. htmlspecialchars($_GET["price"]).'");');
}

if(htmlspecialchars($_GET["action"]) == 'removeProduct')
{
    $DB->query('DELETE FROM `products` WHERE id = '. htmlspecialchars($_GET["id"]) .';');
}

echo '<br />';
$DB->query('SELECT * FROM `products`;');
//echo json_encode($DB->getAllRows());

$mysimplexml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><products />');
foreach ($DB->getAllRows() as $item){
    $mysimplexml->addChild('id', $item[0] . " ");
    $mysimplexml->addChild('name', $item[1] . " ");
    $mysimplexml->addChild('price', $item[2] . " ");
}
echo $mysimplexml->asXML();

?>