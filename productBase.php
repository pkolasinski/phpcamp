<?php

/**
 * Created by PhpStorm.
 * User: Paweł
 * Date: 2017-05-30
 * Time: 11:19
 */
abstract class ProductBase {

    protected $_id;
    protected $_name;
    protected $_price;
    protected $_currency;
    protected $_description;
    protected $_images;
    protected $_manufacturer;
    protected $_category;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->_price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->_price = $price;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->_currency;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->_currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->_images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->_images = $images;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->_manufacturer;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->_manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->_category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->_category = $category;
    }

    public function __set($name, $value)
    {
        //if (property_exists($this, $name)) {
            $this->$name = $value;
        //}
    }

    public function __get($name)
    {
        //if (property_exists($this, $name)) {
            return $this->$name;
        //}
    }

    public function __call($name, $arguments){

        $actualMethod = 'set'.$name;
        call_user_func_array(array($this, $actualMethod), $arguments);
    }

}